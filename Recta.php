<?php

/**
 * Class Recta
 *
 * Formula de la recta: Y = mX + b
 */
class Recta
{
    private $punto1;
    private $punto2;

    public function __construct($punto1, $punto2)
    {
        $this->punto1 = $punto1;
        $this->punto2 = $punto2;
    }

    public function puntoPerteneceARecta($punto)
    {
        $m = $this->getPendiente();
        $b = $this->getCruceEjeY($m);

        return ($punto[1] == ($m * $punto[0] + $b));
    }

    public function getPendiente()
    {
        if ($this->punto2[0] - $this->punto1[0] == 0) {
            return 0;
        }

        return round(($this->punto2[1] - $this->punto1[1]) / ($this->punto2[0] - $this->punto1[0]), 2);
    }

    private function getCruceEjeY($m)
    {
        $x = $this->punto1[0];
        $y = $this->punto1[1];

        return round($y - $m * $x, 2);
    }
}