<?php

class Planeta
{
    private $distancia; // kilometros
    private $velocidad; // grados/dia
    private $anguloInicial = 90;

    public function __construct($distancia, $velocidad)
    {
        $this->distancia = $distancia;
        $this->velocidad = $velocidad;

    }

    /**
     * Obtengo las coordenadas del planeta para un dia dado en formato Array [x,y]
     *
     * @param $dia
     * @return array
     */
    public function getCoordenadas($dia)
    {
        $x = round($this->getPosicionAngularX($dia), 2);
        $y = round($this->getPosicionAngularY($dia), 2);

        return [$x, $y];
    }

    /**
     * Obtengo la posicion angular en X para un dia dado
     * Al angulo inicial le resto la velocidad angular porque gira en sentido horario
     *
     * @param $dia
     * @return float|int
     */
    private function getPosicionAngularX($dia)
    {
        return $this->distancia * cos(deg2rad($this->anguloInicial - ($this->velocidad * $dia)));
    }

    /**
     * Obtengo la posicion angular en Y para un día dado
     * Al angulo inicial le resto la velocidad angular porque gira en sentido horario
     *
     * @param $dia
     * @return float|int
     */
    private function getPosicionAngularY($dia)
    {
        return $this->distancia * sin(deg2rad($this->anguloInicial - ($this->velocidad * $dia)));
    }
}
