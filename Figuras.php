<?php

class Figuras
{
    private $posPlaneta1;
    private $posPlaneta2;
    private $posPlaneta3;
    private $posSol;

    public function __construct($posPlaneta1, $posPlaneta2, $posPlaneta3, $posSol)
    {
        $this->posPlaneta1 = $posPlaneta1;
        $this->posPlaneta2 = $posPlaneta2;
        $this->posPlaneta3 = $posPlaneta3;
        $this->posSol = $posSol;
    }

    /**
     * Verificamos si las rectas están alineadas basandonos en si la pendiente de estas
     * son iguales, dándole un delta de 0.04 ya que el sin/cos da muchos decimales y está redondeado
     *
     * @param $recta1
     * @param $recta2
     * @return bool
     */
    public function rectasAlineadas($recta1, $recta2)
    {
        $pendiente1 = abs($recta1->getPendiente());
        $pendiente2 = abs($recta2->getPendiente());

        return abs($pendiente1 - $pendiente2) < 0.04;
    }

    public function alineadosAlSol($recta)
    {
        $coordSol = [0, 0];

        return $recta->puntoPerteneceARecta($coordSol);
    }

    /**
     * Calculamos el area contenida por el triangulo formado por tres puntos
     * usando determinantes con la regla de Sarrus.
     * Ref: https://www.geoan.com/triangulos/area.html
     *
     * @param $punto1
     * @param $punto2
     * @param $punto3
     */
    public function calcularArea($punto1, $punto2, $punto3)
    {
        $area = (
            $punto1[0] * ($punto2[1] - $punto3[1]) +
            $punto2[0] * ($punto3[1] - $punto1[1]) +
            $punto3[0] * ($punto1[1] - $punto2[1])
        );

        return abs($area) / 2;
    }

    /**
     * Verificamos si el sol está contenido dentro del triangulo formado por los tres planetas
     *
     * @return bool
     */
    public function solPerteneceAlTriangulo()
    {
        // 1) Calculamos el area del triangulo comprendido entre los 3 planetas
        $areaTrianguloPlanetas = $this->calcularArea($this->posPlaneta1, $this->posPlaneta2, $this->posPlaneta3);

        // 2) Calculamos el area del triangulo formado por Ferengui-Betasoide-Sol
        $areaPlaneta12Sol = $this->calcularArea($this->posPlaneta1, $this->posPlaneta2, $this->posSol);

        // 3) Calculamos el area del triangulo formado por Ferengui-Vulcano-Sol
        $areaPlaneta13Sol = $this->calcularArea($this->posPlaneta1, $this->posPlaneta3, $this->posSol);

        // 4) Calculamos el area del triangulo formado por Vulcano-Betasoide-Sol
        $areaPlaneta23Sol = $this->calcularArea($this->posPlaneta2, $this->posPlaneta3, $this->posSol);

        // Sumamos las subareas
        $subAreas = $areaPlaneta12Sol + $areaPlaneta13Sol + $areaPlaneta23Sol;

        return $areaTrianguloPlanetas == $subAreas;
    }

    /**
     * Verificamos si el triangulo formado por los tres planetas es el perimetro maximo
     *
     * @param $distanciaMaxima
     * @return bool
     */
    public function trianguloPerimetroMaximo($distanciaMaxima)
    {
        $distanciaEntre12 = $this->calcularDistancia($this->posPlaneta1, $this->posPlaneta2);
        $distanciaEntre13 = $this->calcularDistancia($this->posPlaneta1, $this->posPlaneta3);
        $distanciaEntre23 = $this->calcularDistancia($this->posPlaneta2, $this->posPlaneta3);

        $distanciaTotal = $distanciaEntre12 + $distanciaEntre13 + $distanciaEntre23;

        return round($distanciaTotal, 11) == round($distanciaMaxima, 11);
    }

    /**
     * Calculamos la distancia entre dos puntos
     *
     * @param $punto1
     * @param $punto2
     * @return float
     */
    private function calcularDistancia($punto1, $punto2)
    {
        return sqrt(
            abs($punto2[0] - $punto1[0]) ^ 2 +
            abs($punto2[1] - $punto1[1]) ^ 2
        );
    }
}